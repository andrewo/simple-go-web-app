## How to build (get going )

### Infrastructure build

Note: for simplicity sake - these instructions assume a fresh build, thus the backend.tf has already been disabled.  
We could keep the backend/state and maintain the infrastructure build/state within the cicd pipeline (personally I would avoid this)

```
cd ./terraform 
terraform init
terraform plan
terraform apply
```

From a successful terraform apply, the output values will be shown to screen like so:

```
Outputs:

app_sg = sg-0a3a80ce571dcaXXX
app_subnet_a = subnet-0701ec15634572XXX
app_subnet_b = subnet-008edea26bc976XXX
app_subnet_c = subnet-005ca8a02b8052XXX
target_group_arn = arn:aws:elasticloadbalancing:ap-southeast-2:849828677XXX:targetgroup/assembly-nonprod/930c3e22d8badXXX
```

### Setup the bitbucket pipelines
Use the values from the infrastructure build to populate the deployment variables in bitbucket pipeline, an example is shown in ./output/deployment_variables.png and ./output/repository_variables.png  

Please note for target_group_arn value, any forward slash will need to be escaped

From the AWS IAM console create a key and secret key for assembly_pipeline user and populate that into the deployment variables aws_access_key_id and aws_secret_access_key

Populate the deployment variable desired_count with 2 (or the number of desired running containers at one time)

Populate the repository variables docker_login_username and docker_login_password for your docker hub account

### Merge/push to master branch
Any changes to master branch will trigger bitbucket pipelines to perform a complete CI and CD to a testing environment in ECS.  
bitbucket-pipelines.yml can be changed for non trivial workflows and deployments, this is highly encouraged

## Bonus Points
### Architecture
HA AWS ALB -> 1 ECS service supervising/managing 2+ FARGATE tasks/containers across 3 AZ  

VPC: 1 vpc with multi AZ subnets for public (ALBs), private (ECS/FARGATE) and db  

We chose FARGATE over ECS and EKS since within AWS ECS and EKS requires building of nodes. Also EKS requires $ for the managed master cluster  

Essentially any "permanent" resource built by terraforms, anything container related (ephemeral) is built by the cicd pipeline

### Code Pipeline
We use bitbucket pipelines because there is a free tier, it's saas, integrates nicely with bitbucket/provider, stores integration secrets/values and has nice web GUI.

Configuration is via the bitbucket-pipelines.yml file.

The pipeline is clunkier than is should be with (using sed and files). This is because the AWS cli has a bug where it won't parse square brackets in the cli arguments (which is needed as it's trying to mimic --cli-input-json and json). So template files have been used instead.

Please find the cicd output logs as ./output/pipelineLog-XX.txt
