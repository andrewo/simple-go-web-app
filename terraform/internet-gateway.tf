resource "aws_internet_gateway" "gw" {
    vpc_id = "${aws_vpc.assembly.id}"
    tags { Name = "assembly_igw" }
}
