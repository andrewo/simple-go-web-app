resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.assembly.id}"
  tags { Name = "public" }
}

resource "aws_route" "public_default_route" {
    route_table_id = "${aws_route_table.public.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
}

resource "aws_route_table_association" "public_a_rta" {
    subnet_id = "${aws_subnet.public_a_subnet.id}"
    route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "public_b_rta" {
    subnet_id = "${aws_subnet.public_b_subnet.id}"
    route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "public_c_rta" {
    subnet_id = "${aws_subnet.public_c_subnet.id}"
    route_table_id = "${aws_route_table.public.id}"
}
