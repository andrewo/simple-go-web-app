resource "aws_security_group" "app_sg" {
    #name, name_prefix and decription deliberately not included, please use tags as identifiers
    vpc_id = "${aws_vpc.assembly.id}"

    ingress = {
        from_port = 8080
        to_port = 8080
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress = {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags {
        Name = "app_sg"
        description = "app_sg"
        direction = "both"
    }
}

output "app_sg" {
    value = "${aws_security_group.app_sg.id}"
}
