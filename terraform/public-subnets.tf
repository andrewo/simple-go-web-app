resource "aws_subnet" "public_a_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "public_a_subnet")}"
  availability_zone = "ap-southeast-2a"  
    tags {
        Name = "public_a"
    }
}

resource "aws_subnet" "public_b_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "public_b_subnet")}"
  availability_zone = "ap-southeast-2b"  
    tags {
        Name = "public_b"
    }
}

resource "aws_subnet" "public_c_subnet" {
  vpc_id = "${aws_vpc.assembly.id}"
  cidr_block = "${lookup(var.subnet_cidr, "public_c_subnet")}"
  availability_zone = "ap-southeast-2c"   
    tags {
        Name = "public_c"
    }
}
