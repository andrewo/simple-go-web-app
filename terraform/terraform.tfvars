cidr_block = "10.200.0.0/16"

subnet_cidr = {
    public_a_subnet       = "10.200.1.0/24"
    public_b_subnet       = "10.200.2.0/24"
    public_c_subnet       = "10.200.3.0/24"
    app_private_a_subnet  = "10.200.7.0/24"
    app_private_b_subnet  = "10.200.8.0/24"
    app_private_c_subnet  = "10.200.9.0/24"
    db_private_a_subnet   = "10.200.10.0/24"
    db_private_b_subnet   = "10.200.11.0/24"
    db_private_c_subnet   = "10.200.12.0/24"
}
