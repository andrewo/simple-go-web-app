+ umask 000

+ GIT_LFS_SKIP_SMUDGE=1 git clone --branch="master" --depth 50 https://x-token-auth:$REPOSITORY_OAUTH_ACCESS_TOKEN@bitbucket.org/andrewo/simple-go-web-app.git $BUILD_DIR
Cloning into '/opt/atlassian/pipelines/agent/build'...

+ git reset --hard e2dbbdc8b41d07594fe49cd4ff7767235354d4df
HEAD is now at e2dbbdc fixing up bb pipelines

+ git config user.name bitbucket-pipelines

+ git config user.email commits-noreply@bitbucket.org

+ git config push.default current

+ git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://localhost:29418/

+ git remote set-url origin http://bitbucket.org/andrewo/simple-go-web-app

+ chmod 777 $BUILD_DIR

+ docker login -u $docker_login_username -p $docker_login_password
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

+ docker pull ${docker_login_username}/${BITBUCKET_REPO_SLUG}:${BITBUCKET_BUILD_NUMBER}
16: Pulling from ahaiong/simple-go-web-app
6e410858abb7: Pulling fs layer
6e410858abb7: Verifying Checksum
6e410858abb7: Download complete
6e410858abb7: Pull complete
Digest: sha256:350beb17fed4847c9aa6084686e210d6ee1e5e2d789288f79629301847593889
Status: Downloaded newer image for ahaiong/simple-go-web-app:16

+ docker images
REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE
ahaiong/simple-go-web-app   16                  d5f68ec517d4        19 seconds ago      7.31MB

+ sed -i "s/%%BITBUCKET_REPO_SLUG%%/${BITBUCKET_REPO_SLUG}/g" ecs/task_definition.json

+ sed -i "s/%%BITBUCKET_BUILD_NUMBER%%/${BITBUCKET_BUILD_NUMBER}/g" ecs/task_definition.json

+ sed -i "s/%%DOCKER_LOGIN_USERNAME%%/${docker_login_username}/g" ecs/task_definition.json

+ cat ecs/task_definition.json
{
"containerDefinitions": [
  {
    "name": "simple-go-web-app",
    "privileged": false,
    "image": "ahaiong/simple-go-web-app:16",
    "cpu": 256,
    "memory": 512,
    "memoryReservation": 512,
    "essential": true,
    "workingDirectory": "/app",
    "portMappings": [
      {
        "hostPort": 8080,
        "protocol": "tcp",
        "containerPort": 8080
      }
    ]
  }
],
  "family": "simple-go-web-app",
  "requiresCompatibilities": [
    "FARGATE"
],
  "executionRoleArn": "arn:aws:iam::849828677909:role/ecsTaskExecutionRole",
  "networkMode": "awsvpc",
  "cpu": "256",
  "memory": "512"
}

+ aws --version
aws-cli/1.16.90 Python/3.6.5 Linux/4.19.23-coreos-r1 botocore/1.12.80

+ export AWS_DEFAULT_REGION=ap-southeast-2

+ export AWS_ACCESS_KEY_ID=${aws_access_key_id}

+ export AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}

+ echo "register a new task definition"
register a new task definition

+ export TASK_VERSION=$(aws ecs register-task-definition --cli-input-json file://ecs/task_definition.json | jq --raw-output '.taskDefinition.revision')

+ echo "task definition to use is $TASK_VERSION"
task definition to use is 15

+ echo "upsert a new ecs service, let ecs perform the rest"
upsert a new ecs service, let ecs perform the rest

+ sed -i "s/%%BITBUCKET_REPO_SLUG%%/${BITBUCKET_REPO_SLUG}/g" ecs/service_definition.json

+ sed -i "s/%%TASK_VERSION%%/${TASK_VERSION}/g" ecs/service_definition.json

+ sed -i "s/%%TARGET_GROUP_ARN%%/${target_group_arn}/g" ecs/service_definition.json

+ sed -i "s/%%APP_SUBNET_A%%/${app_subnet_a}/g" ecs/service_definition.json

+ sed -i "s/%%APP_SUBNET_B%%/${app_subnet_b}/g" ecs/service_definition.json

+ sed -i "s/%%APP_SUBNET_C%%/${app_subnet_c}/g" ecs/service_definition.json

+ sed -i "s/%%APP_SG%%/${app_sg}/g" ecs/service_definition.json

+ cat ecs/service_definition.json
{
        "serviceName": "simple-go-web-app",
        "cluster": "assembly",
        "loadBalancers": [
            {
                "targetGroupArn": "arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532",
                "containerName": "simple-go-web-app",
                "containerPort": 8080
            }
        ],
        "desiredCount": 2,
        "launchType": "FARGATE",
        "platformVersion": "LATEST",
        "taskDefinition": "arn:aws:ecs:ap-southeast-2:849828677909:task-definition/simple-go-web-app:15",
        "deploymentConfiguration": {
            "maximumPercent": 200,
            "minimumHealthyPercent": 100
        },
        "networkConfiguration": {
            "awsvpcConfiguration": {
                "subnets": [
                    "subnet-0701ec156345721ae",
                    "subnet-008edea26bc976f51",
                    "subnet-005ca8a02b8052c0b"
                ],
                "securityGroups": [
                    "sg-0a3a80ce571dca93e"
                ],
                "assignPublicIp": "DISABLED"
            }
        },
        "healthCheckGracePeriodSeconds": 0,
        "schedulingStrategy": "REPLICA",
        "enableECSManagedTags": false
}

+ aws ecs list-services --cluster assembly | grep ${BITBUCKET_REPO_SLUG} || aws ecs create-service --service-name $BITBUCKET_REPO_SLUG --cli-input-json file://ecs/service_definition.json
        "arn:aws:ecs:ap-southeast-2:849828677909:service/simple-go-web-app"

+ aws ecs update-service --cluster assembly --service $BITBUCKET_REPO_SLUG --task-definition ${BITBUCKET_REPO_SLUG}:${TASK_VERSION} --desired-count $desired_count
{
    "service": {
        "serviceArn": "arn:aws:ecs:ap-southeast-2:849828677909:service/simple-go-web-app",
        "serviceName": "simple-go-web-app",
        "clusterArn": "arn:aws:ecs:ap-southeast-2:849828677909:cluster/assembly",
        "loadBalancers": [
            {
                "targetGroupArn": "arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532",
                "containerName": "simple-go-web-app",
                "containerPort": 8080
            }
        ],
        "serviceRegistries": [],
        "status": "ACTIVE",
        "desiredCount": 2,
        "runningCount": 4,
        "pendingCount": 0,
        "launchType": "FARGATE",
        "platformVersion": "LATEST",
        "taskDefinition": "arn:aws:ecs:ap-southeast-2:849828677909:task-definition/simple-go-web-app:15",
        "deploymentConfiguration": {
            "maximumPercent": 200,
            "minimumHealthyPercent": 100
        },
        "deployments": [
            {
                "id": "ecs-svc/9223370485234875214",
                "status": "PRIMARY",
                "taskDefinition": "arn:aws:ecs:ap-southeast-2:849828677909:task-definition/simple-go-web-app:15",
                "desiredCount": 2,
                "pendingCount": 0,
                "runningCount": 0,
                "createdAt": 1551619900.593,
                "updatedAt": 1551619900.593,
                "launchType": "FARGATE",
                "platformVersion": "1.3.0",
                "networkConfiguration": {
                    "awsvpcConfiguration": {
                        "subnets": [
                            "subnet-008edea26bc976f51",
                            "subnet-0701ec156345721ae",
                            "subnet-005ca8a02b8052c0b"
                        ],
                        "securityGroups": [
                            "sg-0a3a80ce571dca93e"
                        ],
                        "assignPublicIp": "DISABLED"
                    }
                }
            },
            {
                "id": "ecs-svc/9223370485235041155",
                "status": "ACTIVE",
                "taskDefinition": "arn:aws:ecs:ap-southeast-2:849828677909:task-definition/simple-go-web-app:14",
                "desiredCount": 2,
                "pendingCount": 0,
                "runningCount": 2,
                "createdAt": 1551619734.652,
                "updatedAt": 1551619831.987,
                "launchType": "FARGATE",
                "platformVersion": "1.3.0",
                "networkConfiguration": {
                    "awsvpcConfiguration": {
                        "subnets": [
                            "subnet-008edea26bc976f51",
                            "subnet-0701ec156345721ae",
                            "subnet-005ca8a02b8052c0b"
                        ],
                        "securityGroups": [
                            "sg-0a3a80ce571dca93e"
                        ],
                        "assignPublicIp": "DISABLED"
                    }
                }
            },
            {
                "id": "ecs-svc/9223370485235187142",
                "status": "ACTIVE",
                "taskDefinition": "arn:aws:ecs:ap-southeast-2:849828677909:task-definition/simple-go-web-app:12",
                "desiredCount": 2,
                "pendingCount": 0,
                "runningCount": 1,
                "createdAt": 1551619588.665,
                "updatedAt": 1551619690.392,
                "launchType": "FARGATE",
                "platformVersion": "1.3.0",
                "networkConfiguration": {
                    "awsvpcConfiguration": {
                        "subnets": [
                            "subnet-008edea26bc976f51",
                            "subnet-0701ec156345721ae",
                            "subnet-005ca8a02b8052c0b"
                        ],
                        "securityGroups": [
                            "sg-0a3a80ce571dca93e"
                        ],
                        "assignPublicIp": "DISABLED"
                    }
                }
            },
            {
                "id": "ecs-svc/9223370485235944738",
                "status": "ACTIVE",
                "taskDefinition": "arn:aws:ecs:ap-southeast-2:849828677909:task-definition/simple-go-web-app:11",
                "desiredCount": 2,
                "pendingCount": 0,
                "runningCount": 1,
                "createdAt": 1551618831.069,
                "updatedAt": 1551619677.89,
                "launchType": "FARGATE",
                "platformVersion": "1.3.0",
                "networkConfiguration": {
                    "awsvpcConfiguration": {
                        "subnets": [
                            "subnet-008edea26bc976f51",
                            "subnet-0701ec156345721ae",
                            "subnet-005ca8a02b8052c0b"
                        ],
                        "securityGroups": [
                            "sg-0a3a80ce571dca93e"
                        ],
                        "assignPublicIp": "DISABLED"
                    }
                }
            }
        ],
        "roleArn": "arn:aws:iam::849828677909:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS",
        "events": [
            {
                "id": "d0db1b07-f2a4-4c01-a607-4c01edb66f45",
                "createdAt": 1551619844.527,
                "message": "(service simple-go-web-app) has begun draining connections on 2 tasks."
            },
            {
                "id": "378ffd2d-5f6b-4297-86b7-1d49738a3a32",
                "createdAt": 1551619844.526,
                "message": "(service simple-go-web-app) deregistered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "ce4e0169-838c-4f26-a591-6aaa0e781803",
                "createdAt": 1551619808.85,
                "message": "(service simple-go-web-app) registered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "0a3406f7-6479-4360-95e3-8d1c525310bb",
                "createdAt": 1551619770.995,
                "message": "(service simple-go-web-app) has started 1 tasks: (task a1755a9d-a1f8-47ea-844e-c01eec0616e7)."
            },
            {
                "id": "62758fcd-0e4a-403c-a984-65c2e454fbf5",
                "createdAt": 1551619758.961,
                "message": "(service simple-go-web-app) has started 1 tasks: (task ee086ba5-d955-4234-a966-c0c66ed30658)."
            },
            {
                "id": "2580e9d3-d35c-4ec3-8d84-80e12c7d8e62",
                "createdAt": 1551619746.814,
                "message": "(service simple-go-web-app) has stopped 1 running tasks: (task 1bcc1dec-fd44-438d-b019-98bdc3aa0ddd)."
            },
            {
                "id": "59d80ee7-97c4-4e52-b082-ce440d693bfb",
                "createdAt": 1551619737.68,
                "message": "(service simple-go-web-app) has stopped 1 running tasks: (task 56a134de-92b9-4694-ae93-b8c67dc593d8)."
            },
            {
                "id": "a732014f-dc20-412e-b12c-1d8b7a20540b",
                "createdAt": 1551619677.934,
                "message": "(service simple-go-web-app) has begun draining connections on 1 tasks."
            },
            {
                "id": "7129ca14-3bc3-4ea2-b3d1-91250d96c48f",
                "createdAt": 1551619677.933,
                "message": "(service simple-go-web-app) deregistered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "756af3de-e954-4c24-b5a9-4a38e036b658",
                "createdAt": 1551619665.759,
                "message": "(service simple-go-web-app) has begun draining connections on 1 tasks."
            },
            {
                "id": "b85e9b7a-6a66-4d18-8716-5bfffde994ad",
                "createdAt": 1551619665.758,
                "message": "(service simple-go-web-app) deregistered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "18712ec9-6dff-4194-b8e2-0c59631fb8ab",
                "createdAt": 1551619630.243,
                "message": "(service simple-go-web-app) registered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "7cb31da9-3712-4187-8001-87eb87630f5d",
                "createdAt": 1551619606.773,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 1bcc1dec-fd44-438d-b019-98bdc3aa0ddd) (task 0972a10d-c348-448b-945c-26794a7ba3b8)."
            },
            {
                "id": "7a9f2a5f-efbf-4efc-82f9-434f57a738db",
                "createdAt": 1551619016.549,
                "message": "(service simple-go-web-app) has reached a steady state."
            },
            {
                "id": "6cc5cef3-9a8b-4d65-bf6f-d8a62a12e9d6",
                "createdAt": 1551618992.534,
                "message": "(service simple-go-web-app) has stopped 2 running tasks: (task 600aa379-39df-4acd-83c7-0c02a1adb080) (task 9213dee9-daf3-4bbd-816a-cc8f79c48ec3)."
            },
            {
                "id": "2d83b1ce-f814-44b3-9d59-267b6145b18f",
                "createdAt": 1551618922.153,
                "message": "(service simple-go-web-app) has begun draining connections on 2 tasks."
            },
            {
                "id": "15a35c17-ecc2-4701-a55a-34e7df63af2b",
                "createdAt": 1551618922.152,
                "message": "(service simple-go-web-app) deregistered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "bb07200f-b86d-409f-bbcd-df4f5b5e74d3",
                "createdAt": 1551618875.704,
                "message": "(service simple-go-web-app) registered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "c4b25ee2-57e2-4310-aab9-2f4f44224ad2",
                "createdAt": 1551618850.879,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 56a134de-92b9-4694-ae93-b8c67dc593d8) (task 20bdc241-d5f0-47e3-85ba-5d4316b74c15)."
            },
            {
                "id": "60aa5b38-68f7-45b9-b52e-84cdc7eb2839",
                "createdAt": 1551618756.665,
                "message": "(service simple-go-web-app) has reached a steady state."
            },
            {
                "id": "c3a35b6e-d81d-4cdc-948b-acd0ecbc3def",
                "createdAt": 1551618733.077,
                "message": "(service simple-go-web-app) has stopped 2 running tasks: (task 3b5bd18d-71bb-494e-882d-a5ee2950f1de) (task 2d5ee638-8cfe-49ba-8691-67b8df984719)."
            },
            {
                "id": "67510986-2dbb-4ecc-904e-c26635e40a26",
                "createdAt": 1551618663.569,
                "message": "(service simple-go-web-app) has begun draining connections on 2 tasks."
            },
            {
                "id": "7dc1a3eb-b655-422c-8b0d-2dfefde33d42",
                "createdAt": 1551618663.568,
                "message": "(service simple-go-web-app) deregistered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "9139c43f-75ef-4432-a801-2c3fc4ede67a",
                "createdAt": 1551618617.432,
                "message": "(service simple-go-web-app) registered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "de3bdee2-ec40-4cf3-89fa-4def434cbc7a",
                "createdAt": 1551618595.976,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 9213dee9-daf3-4bbd-816a-cc8f79c48ec3) (task 600aa379-39df-4acd-83c7-0c02a1adb080)."
            },
            {
                "id": "40ee7cd1-8be9-40e2-a1bc-ea313d5c57b3",
                "createdAt": 1551616582.318,
                "message": "(service simple-go-web-app) has reached a steady state."
            },
            {
                "id": "58b1f7a2-997e-4629-87e9-024654c0e15b",
                "createdAt": 1551616558.893,
                "message": "(service simple-go-web-app) has stopped 2 running tasks: (task 1582cf7e-9389-4960-92fa-0e3e74c0ad37) (task f1a40b18-33a4-408e-8b68-c7e5f92f2687)."
            },
            {
                "id": "d46295ac-c00e-4aee-a706-f7f5c75b4ace",
                "createdAt": 1551616487.116,
                "message": "(service simple-go-web-app) has begun draining connections on 2 tasks."
            },
            {
                "id": "9cb3e135-9ad8-41d5-8bb0-d627c39a1662",
                "createdAt": 1551616487.115,
                "message": "(service simple-go-web-app) deregistered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "b83e93f9-ee40-4fa2-a13a-1354bbb41a81",
                "createdAt": 1551616439.244,
                "message": "(service simple-go-web-app) registered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "2618617d-b878-47eb-a6a5-1d4ae5d95920",
                "createdAt": 1551616417.153,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 3b5bd18d-71bb-494e-882d-a5ee2950f1de) (task 2d5ee638-8cfe-49ba-8691-67b8df984719)."
            },
            {
                "id": "78eaa11e-64d6-4769-971d-e163e777c612",
                "createdAt": 1551616160.556,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 104777a9-3b61-4b92-9eca-b1d41f90b1f9) (task 9907aad2-764d-4ca6-a57d-621ae4e246b6)."
            },
            {
                "id": "5e7c611c-fa49-4a3a-8d26-a94874aa76bc",
                "createdAt": 1551616092.282,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 4f3d54c7-dfec-4c4c-819d-15eca0630346) (task 392058b1-e1f5-4a56-b8f4-713d49524cb0)."
            },
            {
                "id": "73870681-c611-4245-a468-c5954b265017",
                "createdAt": 1551615489.912,
                "message": "(service simple-go-web-app) has reached a steady state."
            },
            {
                "id": "478e5373-2e44-4dc7-b2f6-490cafe1648e",
                "createdAt": 1551615466.791,
                "message": "(service simple-go-web-app) has stopped 2 running tasks: (task d48a7abd-862a-4c8c-89c6-523cf47ec5ea) (task f8a231b2-388e-4d1b-92ec-3beff793f4c3)."
            },
            {
                "id": "174aa3ce-46e1-48d8-953b-057916ceb29a",
                "createdAt": 1551615396.354,
                "message": "(service simple-go-web-app) has begun draining connections on 2 tasks."
            },
            {
                "id": "c3076293-8715-4509-8fa4-7732b4f1b2a3",
                "createdAt": 1551615396.353,
                "message": "(service simple-go-web-app) deregistered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "e83480b6-4f6f-45d8-9e50-9ab3971c7fce",
                "createdAt": 1551615349.896,
                "message": "(service simple-go-web-app) registered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "7e6032b4-940b-4d51-8db7-464ba724877c",
                "createdAt": 1551615325.786,
                "message": "(service simple-go-web-app) has started 2 tasks: (task f1a40b18-33a4-408e-8b68-c7e5f92f2687) (task 1582cf7e-9389-4960-92fa-0e3e74c0ad37)."
            },
            {
                "id": "14f5f340-a266-4c3e-b01d-304ea8d4676d",
                "createdAt": 1551609866.16,
                "message": "(service simple-go-web-app) has reached a steady state."
            },
            {
                "id": "f9944097-0c46-4611-916e-455289df4c07",
                "createdAt": 1551609843.333,
                "message": "(service simple-go-web-app) registered 2 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "a757b9c7-b1ce-4374-acf7-1aa3a21796f2",
                "createdAt": 1551609821.927,
                "message": "(service simple-go-web-app) has started 2 tasks: (task f8a231b2-388e-4d1b-92ec-3beff793f4c3) (task d48a7abd-862a-4c8c-89c6-523cf47ec5ea)."
            },
            {
                "id": "4f6fe054-90ff-40df-ad0c-f4f6d97a9ae0",
                "createdAt": 1551609787.976,
                "message": "(service simple-go-web-app) deregistered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "19290342-9807-4421-8cdf-f3071c820998",
                "createdAt": 1551609787.955,
                "message": "(service simple-go-web-app) has begun draining connections on 1 tasks."
            },
            {
                "id": "14e85329-2922-4838-956a-656bb73bb371",
                "createdAt": 1551609787.946,
                "message": "(service simple-go-web-app) deregistered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "1e0e3958-d76a-4b7d-acdd-0804c62eeb2b",
                "createdAt": 1551609346.005,
                "message": "(service simple-go-web-app) has reached a steady state."
            },
            {
                "id": "c6e36ea5-0c5a-432f-b7bc-5bec1ae3cf8a",
                "createdAt": 1551609321.843,
                "message": "(service simple-go-web-app) registered 1 targets in (target-group arn:aws:elasticloadbalancing:ap-southeast-2:849828677909:targetgroup/assembly-nonprod/930c3e22d8bad532)"
            },
            {
                "id": "18cdd741-4029-45a6-b71b-f055cc5052f4",
                "createdAt": 1551609299.561,
                "message": "(service simple-go-web-app) has started 2 tasks: (task 88d6d3ab-47a1-4384-9b8b-8d32bd55ce8e) (task 5762c92b-adee-4c4e-b343-aa1ae5bc2793)."
            }
        ],
        "createdAt": 1551609290.618,
        "placementConstraints": [],
        "placementStrategy": [],
        "networkConfiguration": {
            "awsvpcConfiguration": {
                "subnets": [
                    "subnet-008edea26bc976f51",
                    "subnet-0701ec156345721ae",
                    "subnet-005ca8a02b8052c0b"
                ],
                "securityGroups": [
                    "sg-0a3a80ce571dca93e"
                ],
                "assignPublicIp": "DISABLED"
            }
        },
        "healthCheckGracePeriodSeconds": 0,
        "schedulingStrategy": "REPLICA",
        "enableECSManagedTags": false,
        "propagateTags": "NONE"
    }
}
Searching for test report files in directories named [test-results, failsafe-reports, test-reports, surefire-reports] down to a depth of 4
Finished scanning for test reports. Found 0 test report files.
Merged test suites, total number tests is 0, with 0 failures and 0 errors.
