FROM golang:alpine AS builder

RUN mkdir /app 
COPY ./src /app 

WORKDIR /app 

RUN CGO_ENABLED=0 go build -o main main.go

FROM scratch

COPY --from=builder /app/main /app/main

EXPOSE 8080

CMD ["/app/main"]
